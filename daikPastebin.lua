local args = {...}

local function saveFromHTTP(link, filename)
	local h = fs.open(filename, "w")
	h.write(http.get(link).readAll())
	h.close()
end

local function main()
	local link
	local file
	if agrs[1] == "get" then
		local link = "https://pastebin.com/raw/" .. args[2]
		local file = args[3]
	else
		local link = "https://pastebin.com/raw/" .. args[1]
		local file = args[2]
	end
	saveFromHTTP(link, file)
end

main()